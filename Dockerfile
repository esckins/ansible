# This file is a template, and might need editing before it works on your project.
FROM python:latest

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        sshpass \
    && rm -rf /var/lib/apt/lists/*

COPY parse.py /usr/local/bin/authparser
RUN chmod +x /usr/local/bin/authparser

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
RUN pip install ansible envtpl pipenv requests --no-cache

ENV ANSIBLE_HOST_KEY_CHECKING=False
ENV ANSIBLE_INVENTORY=./hosts
ENV ANSIBLE_FORCE_COLOR=True
