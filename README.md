# Ansible

Have you ever wanted to use GitLab CI as your Ansible orchestrator rather than Ansible Tower? 
This project is aimed at providing Ansible users an easy way to get started with GitLab + Ansible 
without having an in-depth knowledge of GitLab or GitLab CI.

What's in the box:

* A Docker Image with Ansible setup and ready to work inside of GitLab CI.
* A simple GitLab CI that can be referenced within your GitLab Project.
* Some defaults to make your life a little easier.

## Getting Started (basic)

1. Your `.gitlab-ci.yml` file:

```
include:
  - remote: https://gitlab.com/poffey21/ansible/raw/master/Ansible.gitlab-ci.yml
```

2. Your repository structure:

* `.gitlab-ci.yml`: Simply contains the `include` statement above
* `hosts`: Your inventory file for servers
* `site.yml`: Your Playbook Manifest
* `roles`: Directory of the roles referenced in your playbook
* `group_vars`: Directory of the group variables referenced through the roles and playbook

3. Your GitLab CI Variables (in `Settings` > `CI/CD` > `Variables`):

* `ANSIBLE_USER`: The username used to log into the servers via SSH
* `ANSIBLE_PASSWORD`: The password used to log into the servers via SSH


## Advanced Configuration Items

* `ANSIBLE_PLAYBOOK`: If you want to override the `site.yml` file name and location, you can set the referenced environment variable.
* `ANSIBLE_FLAGS`: If you want to add additional flags to the ansible command (like `-vvv` to increase verbosity).
* `ANSIBLE_HOSTS`: If you want to override the `hosts` file name and location, you can set the referenced environment variable.

#### Other Options For Logging in

* `ANSIBLE_SSH_PRIVATE_KEY`: Your SSH Private Key used to connect to your hosts
* `ANSIBLE_SSH_PRIVATE_KEY_MASKED`: Your SSH Private Key used to connect to your hosts stored in base64 format
